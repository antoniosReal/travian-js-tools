const start = async function(page) {
    await page.waitForTimeout(3600);
    const listBtn = await page.$('[href="https://ts7.x1.europe.travian.com/build.php?id=39&gid=16&tt=99"]');
    await listBtn.click({force: true})
    await page.waitForLoadState('load');
    await page.waitForTimeout(7400);

    const btn1 = await page.$('[onclick="Travian.Game.RaidList.startRaid(3753)"]');
    await btn1.click({force: true})
    await page.waitForTimeout(2600);

    const btn2 = await page.$('[onclick="Travian.Game.RaidList.startRaid(47)"]');
    await btn2.click({force: true})
    await page.waitForTimeout(4700);

    const btn3 = await page.$('[onclick="Travian.Game.RaidList.startRaid(3794)"]');
    await btn3.click({force: true})
    await page.waitForTimeout(3200);

    const btn4 = await page.$('[onclick="Travian.Game.RaidList.startRaid(4379)"]');
    await btn4.click({force: true})
    await page.waitForTimeout(5200);

    const btn5 = await page.$('[onclick="Travian.Game.RaidList.startRaid(4380)"]');
    await btn5.click({force: true})
}

module.exports = { start }