const start = async function(page, units, time, cords, type, fromVillage) {
    const villages = ['18282', '23549', '27367']

    await page.waitForTimeout(3000);
    var attackDate = new Date(time)
    var attackerVillage = villages[fromVillage]
    await page.waitForTimeout(2000);

    const villageBtn = await page.$('[href="?newdid='+ attackerVillage +'&"]');
    await villageBtn.click({force: true})
    await page.waitForLoadState('load');
    await page.waitForTimeout(2000);

    const dorf2 = await page.$('[href="/dorf2.php"]');
    await dorf2.click({force: true})
    await page.waitForLoadState('load');
    await page.waitForTimeout(2000);

    const militaryBase = await page.$('[data-name="Base Militare"] > a');
    await militaryBase.click({force: true})
    await page.waitForLoadState('load');
    await page.waitForTimeout(2000);

    var x = await page.$('[name="x"]');
    var y = await page.$('[name="y"]');
    await y.type(cords.y);
    await page.waitForTimeout(1000);
    await x.type(cords.x);
    await page.waitForTimeout(1000);

    if(type === 'raid') {
        const raidBtn = await page.$('[name="eventType"][value="4"]');
        await raidBtn.click({force: true})
    } else if (type === 'attack') {
        const attackBtn = await page.$('[name="eventType"][value="3"]');
        await attackBtn.click({force: true})
    }

    if(units.clubs) {
        const clubsInput = await page.$('[name="troop[t1]"]');
        await clubsInput.type(units.clubs);
        await page.waitForTimeout(1000);
    }

    if(units.spy) {
        const spyInput = await page.$('[name="troop[t4]"]');
        await spyInput.type(units.spy);
        await page.waitForTimeout(1000);
    }

    if(units.cata) {
        const cataInput = await page.$('[name="troop[t8]"]');
        await cataInput.type(units.cata);
        await page.waitForTimeout(1000);
    }

    const nextPageBtn = await page.$('[type="submit"][value="ok"]');
    await nextPageBtn.click({force: true})
    await page.waitForLoadState('load');
    await page.waitForTimeout(2000);
    var flag = false;
    var counter = 0;
    
    const doAttack = async(page) => {
        const goBtn = await page.$('.rallyPointConfirm');
        await goBtn.click({force: true})
        await page.waitForLoadState('load');
        await page.waitForTimeout(2000);
        flag = true;
    }

    const repeat = async(page) => {
        if(!flag) {
            var actualTime = await page.textContent('[id="at"]')
            var goTime = attackDate.toISOString().split('T')[1].split('.')[0]

            if(actualTime && goTime) {
                counter++;
    
                console.log(actualTime, 'actual')
                console.log(goTime, 'go')
                console.log('-------------------')
    
                if(actualTime === goTime) {
                    await doAttack(page)
                }
            }
        }
    }

    setInterval(async()=>{
        await repeat(page)
    }, 500)

    await page.waitForTimeout(2147483646);
}

module.exports = { start }