//this is the only variable you need to edit and it's the max price for 5 elements (for example cages or ointments)
var price = 666;

var bid = document.querySelectorAll('.bidButton')
var i = -1;
var n = bid.length;

var interval = setInterval(()=>{
    if(i < n) {
        i++;
        bid[i].click();
        setTimeout(()=>{
            var actual = parseInt(document.querySelector('.silver.selected').textContent)
            switch (actual) {
                case 5:
                    document.querySelector('.maxBid').value = price;
                    document.querySelector('.submitBid').querySelector('button').click()
                    break;

                case 10:
                    document.querySelector('.maxBid').value = price*1.9;
                    document.querySelector('.submitBid').querySelector('button').click()
                    break;

                case 30:
                    document.querySelector('.maxBid').value = price*5.7;
                    document.querySelector('.submitBid').querySelector('button').click()
                    break;

                case 50:
                    document.querySelector('.maxBid').value = price*9.4;
                    document.querySelector('.submitBid').querySelector('button').click()
                    break;

                default:
                    document.querySelector('.switchOpened').click()
                    break;
            }
        }, 300)
    }
}, 1000)


// copy-paste and launch this when you want stop the automatic auctions
clearInterval(interval)
