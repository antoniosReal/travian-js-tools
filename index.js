const playwright = require('playwright');
const autoFarmVillage = require('./features/autoFarmVillage')
const autoAdv = require('./features/adventureTime')
const autoAttackHour = require('./features/autoAttackHour')

const browserType = 'chromium'; 

var page = null;
var browser = null;
var context = null;

async function start() {
    browser = await playwright[browserType].launch({ headless: false, viewport: { width: 1920, height: 1080 }, timeout: 2147483646, globalTimeout:2147483646 });
    context = await browser.newContext({screen: { width: 1920, height: 1080 }, viewport: { width: 1920, height: 1080 }});
    context.setDefaultTimeout(2147483646)
    page = await context.newPage(); 
    
    await page.goto('https://ts7.x1.europe.travian.com/dorf1.php'); // YOUR_SERVER
    await page.waitForLoadState('load');
    await login()
}

async function end() {
    await page.waitForTimeout(3000);
    await browser.close(); 
}

async function login() {
    var name = await page.$('[name="name"]');
    var pass = await page.$('[name="password"]');
    await name.type('test'); // YOUR_USERNAME
    await page.waitForTimeout(1000);
    await pass.type('test'); // YOUR_PASSWORD
    await page.waitForTimeout(1000);
    await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
    await page.waitForTimeout(2000);
    const loginBtn = await page.$('[value="Login"]');
    await loginBtn.click({force: true})
    await page.waitForLoadState('load');
}

async function doFarms() {
    await start();
    await autoFarmVillage.start(page)
    await end();
}

async function doAdventure() {
    await start();
    await autoAdv.start(page)
    await end();
}

async function doAttack() {
    var date = '2023-03-18T00:00:10'

    await start();
    await autoAttackHour.start(page, {clubs: '1', spy: null, cata: null}, date, {x: '5', y: '-35'}, 'reinforce', 1)
    await end();
}

async function main () { // BOT_SETTINGS
    var hoursFarms = 4;
    var msFarms = 1000 * 60 *60 * hoursFarms;

    setInterval(async()=>{
        await doFarms();
    }, msFarms)
}

main();