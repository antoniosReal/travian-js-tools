## Travian JS tools

A simple travian bot, written using playwright.

Feature included:

- Schedule attacks, raids and reinforcements at a specific time
- Automatic hero adventures
- Automatic farmlist sender
- Elephants finder
- Automatic auctions

## Usage

First of all you need to install dependencies:

``npm install``

Then it will be necessary to configure the address of the server where you play, in addition to the login data, you find everything in the index.js file, you can find the right place by looking in the file for these comments:

``YOUR_SERVER``

``YOUR_USERNAME``

``YOUR_PASSWORD``

``BOT_SETTINGS``